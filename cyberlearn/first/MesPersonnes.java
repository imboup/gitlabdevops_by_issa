package testFirst;

import java.io.*;
import java.lang.*;
 
public class MesPersonnes{

	public static void main(String [] args){
		Personne pers=new Personne("issa","MBOUP");
		System.out.print("\t On se presente:");
		System.out.println(pers.sePresenter());
		
		System.out.print("\t Nom:"+pers.getNom());
		System.out.print("\t  Prenom:"+pers.getPrenom());
		
		pers.setPrenom("modou");
		pers.setNom("fall");
			
		System.out.print("\t Nom modifié:"+pers.getNom());
                System.out.print("\t  Prenom modifié:"+pers.getPrenom());
	}
}
