
package testFirst;

import java.io.*; 

public class Personne{
	//public static void main(String [] args){}

private String nom;
private String prenom;

	public Personne(String unNom, String unPrenom){ this.nom=unNom; this.prenom=unPrenom;}

	public String getNom(){	return this.nom;}

	public String getPrenom(){ return this.prenom;}
	
	public void setNom(String unNom){ this.nom=unNom;}
	
	public void setPrenom(String unPrenom){ this.prenom=unPrenom;}

	public String sePresenter(){ return "Bonjour, je m'appelle"+this.getNom();}
	
}
